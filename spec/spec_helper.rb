require 'coveralls'
Coveralls.wear!
#Coveralls.wear!('rails')

require_relative '../lib/library'
require_relative '../lib/book'
require_relative '../lib/elevator'
require_relative '../lib/building'
require_relative '../lib/person'

require 'yaml'
