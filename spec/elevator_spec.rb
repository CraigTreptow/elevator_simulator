require 'spec_helper'

describe Elevator do
  before :each do
    @elevator = Elevator.new(1, 10)
  end

  describe '#new' do
    it 'takes two parameters and returns a Elevator object' do
      expect(@elevator).to be_an_instance_of Elevator
    end
  end

  describe '#current_floor' do
    it 'returns the correct floor' do
      expect(@elevator.current_floor).to eql 1
    end
  end

  describe '#max_occupants' do
    it 'returns the correct maxium occupants' do
      expect(@elevator.max_occupants).to eql 10
    end
  end
end
