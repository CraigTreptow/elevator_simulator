require 'spec_helper'

describe Person do
  before :each do
    @person = Person.new(1, 10)
  end

  describe '#new' do
    it 'takes two parameters and returns a Person object' do
      expect(@person).to be_an_instance_of Person
    end
  end

  describe '#starting_floor' do
    it 'returns the correct starting floor' do
      expect(@person.starting_floor).to eql 1
    end
  end

  describe '#destination_floor' do
    it 'returns the correct destination floor' do
      expect(@person.destination_floor).to eql 10
    end
  end
end
