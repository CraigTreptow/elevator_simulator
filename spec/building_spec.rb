require 'spec_helper'

describe Building do
  before :each do
    @building = Building.new(10, 1)
  end

  describe '#new' do
    it 'takes two parameters and returns a Building object' do
      expect(@building).to be_an_instance_of Building
    end
  end

  describe '#floors' do
    it 'returns the correct number of floors' do
      expect(@building.floors).to eql 10
    end
  end

  describe '#elevators' do
    it 'returns the correct number of elevators' do
      expect(@building.elevators).to eql 1
    end
  end
end
