# Elevator Simulator

### Status
[![Build Status](https://travis-ci.org/ctreptow/elevator_simulator.svg?branch=master)](https://github.com/CraigTreptow/elevator_simulator)

[![Coverage Status](https://img.shields.io/coveralls/CraigTreptow/elevator_simulator.svg)](https://coveralls.io/r/CraigTreptow/elevator_simulator?branch=master)

Elevator Simulation

## Description
The idea is to simulate a known quantity and rate of people going to particular floor(s) in a building.  The purpose of this is to experiment with how and when to send elevators to floors.
