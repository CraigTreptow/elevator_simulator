# Person - takes People objects up and down buildings
class Person
  attr_accessor :starting_floor, :destination_floor

  def initialize(start_floor, end_floor)
    @starting_floor     = start_floor
    @destination_floor  = end_floor
  end
end
