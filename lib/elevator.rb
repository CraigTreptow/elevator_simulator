# Elevator - takes People objects up and down buildings
class Elevator
  attr_accessor :current_floor, :max_occupants

  def initialize(floor, max)
    @current_floor = floor
    @max_occupants = max
  end
end
