# Building - takes People objects up and down buildings
class Building
  attr_accessor :floors, :elevators

  def initialize(num_floors, num_elevators)
    @floors     = num_floors
    @elevators  = num_elevators
  end
end
